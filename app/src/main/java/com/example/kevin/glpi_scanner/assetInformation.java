package com.example.kevin.glpi_scanner;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import org.w3c.dom.Text;

public class assetInformation extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_asset_information);

        Intent intent = getIntent();
        Asset obj = (Asset)intent.getSerializableExtra("text");
        //Toast.makeText(this, obj.toString(), Toast.LENGTH_SHORT).show();


        TextView name = (TextView)findViewById(R.id.nameValue);
        TextView comment = (TextView)findViewById(R.id.commentValue);
        TextView singleplace = (TextView)findViewById(R.id.singlePlaceValue);
        TextView fullplace = (TextView)findViewById(R.id.fullPlaceValue);
        TextView template = (TextView)findViewById(R.id.templateValue);

        name.setText(obj.getDispId());
        comment.setText(obj.getComment());
        singleplace.setText(obj.getSmallLocation());
        fullplace.setText(obj.getFullLocation());
        template.setText(obj.getTemplate());


    }
}
