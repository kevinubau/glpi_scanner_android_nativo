package com.example.kevin.glpi_scanner;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.LinkedList;

import static android.app.Activity.RESULT_OK;


public class Enlistar_fragment extends Fragment {

    static final int BARCODE_REQUEST = 1;  // The request code for the barcode activity

    private EditText barcodeEditText;
    private String code;
    public Connection connection;
    private Asset obj;

    private EditText text;//multiline text
    private LinkedList<String> assetsList;



    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_enlistar, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        assetsList = new LinkedList<>();
        text = getView().findViewById(R.id.listEditText);

        Button copyButton = getView().findViewById(R.id.copyBtn);
        Button clearButton = getView().findViewById(R.id.clearBtn);
        ImageButton scanButton = getView().findViewById(R.id.scanBtn);

        copyButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //text = getView().findViewById(R.id.listEditText);

                ClipboardManager clipboard = (ClipboardManager) getActivity().getSystemService(getContext().CLIPBOARD_SERVICE);
                ClipData clip = ClipData.newPlainText("text", text.getText());
                clipboard.setPrimaryClip(clip);
            }
        });

        clearButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //text = getView().findViewById(R.id.listEditText);
                text.getText().clear();
                assetsList.clear();

            }
        });

        scanButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                /*assetsList.add("hola1\n");
                assetsList.add("hola2\n");
                assetsList.add("hola3\n");
                assetsList.add("hola4\n");

                text.setText("");
                */

                scanCode(v);


            }
        });




    }





    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {

            case (BARCODE_REQUEST) : {



                if(resultCode == RESULT_OK){

                    code = data.getStringExtra("code");


                    this.requestSearch(null);

                    //text.getText().clear();


                }

                break;
            }
        }
    }

    public void scanCode(View view){



        try{

            Toast.makeText(getContext(), "Opening camera...", Toast.LENGTH_SHORT).show();
            Intent camActivity = new Intent(getContext(), Camera_Activity.class);
            startActivityForResult(camActivity, BARCODE_REQUEST);

        }catch (Exception e){

            Toast.makeText(getContext(),"Error opening camera...",Toast.LENGTH_LONG).show();
        }



    }

    public void requestSearch(View view){

        //barcodeEditText = (EditText) getView().findViewById(R.id.barcodeInput);

        System.out.println(code);
        connection = new Connection();
        String url = connection.searchAsset(code);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson= new Gson();
                        obj= gson.fromJson(response.toString(),Asset.class);

                        //assetsList.add(code);
                        //assetsList.add("\t"+obj.getTemplate()+"\n");
                        /*for (String element: assetsList
                                ) {
                            text.getText().append(element);

                        }*/
                        text.getText().append(code+"\t"+obj.getTemplate()+"\n");
                       // callAssetInfo();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();
                        /*
                        assetsList.add(code+"\n");
                        for (String element: assetsList
                                ) {
                            System.out.println(element);
                            text.getText().append(element);

                        }*/
                        text.getText().append(code+"\n");
                    }

                });

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }
}
