package com.example.kevin.glpi_scanner;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {


    static final int BARCODE_REQUEST = 1;
    private DrawerLayout drawer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        android.support.v7.widget.Toolbar toolbar = (android.support.v7.widget.Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if(savedInstanceState == null){ //this is to select by default the scan fragment at the start of the app

            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Scanear_fragment()).commit();

            navigationView.setCheckedItem(R.id.nav_list);


        }


    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {       //handler of drawer options
        switch (item.getItemId()){

            case R.id.nav_list:

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Scanear_fragment()).commit();
                break;
            case R.id.nav_list2:

                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new Enlistar_fragment()).commit();
                break;
            case R.id.nav_list_exit:
                android.os.Process.killProcess(android.os.Process.myPid());
                System.exit(1);
                break;
        }
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {       //to close the drawer if back pressed!
        if(drawer.isDrawerOpen(GravityCompat.START)){
            drawer.closeDrawer(GravityCompat.START);
        }
        else{
            super.onBackPressed();
        }

    }







}
