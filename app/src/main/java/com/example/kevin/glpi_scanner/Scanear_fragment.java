package com.example.kevin.glpi_scanner;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;

import static android.app.Activity.RESULT_OK;

public class Scanear_fragment extends Fragment {

    static final int BARCODE_REQUEST = 1;  // The request code for the barcode activity

    private EditText barcodeEditText;
    private String code;
    private Connection connection;
    private Asset obj;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_scanear, container, false);



    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        barcodeEditText = (EditText) getView().findViewById(R.id.barcodeInput);
        askForPermission();

        connection = new Connection();
        ImageButton button = getView().findViewById(R.id.imageButton);
        Button scanButton = getView().findViewById(R.id.scanBtn);
        button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                scanCode(v);
            }
        });

        scanButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                requestSearch(v);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch(requestCode) {

            case (BARCODE_REQUEST) : {



                if(resultCode == RESULT_OK){

                    code = data.getStringExtra("code");

                    barcodeEditText.setText(code);
                    requestSearch(null);
                }

                break;
            }
        }
    }

    public void requestSearch(View view){

        barcodeEditText = (EditText) getView().findViewById(R.id.barcodeInput);
        code = barcodeEditText.getText().toString();

        String url = connection.searchAsset(code);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest
                (Request.Method.GET, url, null, new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {

                        Gson gson= new Gson();
                        obj= gson.fromJson(response.toString(),Asset.class);

                        callAssetInfo();
                    }
                }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Toast.makeText(getContext(),"Error: "+error.getMessage(),Toast.LENGTH_LONG).show();

                    }

                });

        MySingleton.getInstance(getActivity()).addToRequestQueue(jsonObjectRequest);
    }

    public void scanCode(View view){



        try{

            Toast.makeText(getContext(), "Opening camera...", Toast.LENGTH_SHORT).show();
            Intent camActivity = new Intent(getContext(), Camera_Activity.class);
            startActivityForResult(camActivity, BARCODE_REQUEST);

        }catch (Exception e){

            Toast.makeText(getContext(),"Error opening camera...",Toast.LENGTH_LONG).show();
        }



    }



    public void callAssetInfo(){
        Intent asset = new Intent(getContext(), assetInformation.class);


        asset.putExtra("text", (Serializable) obj);
        startActivity(asset);
    }


    private void askForPermission(){
        // Here, thisActivity is the current activity
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.CAMERA)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.CAMERA)) {

            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.CAMERA},
                        1);

            }
        }


    }
}
