package com.example.kevin.glpi_scanner;

import java.io.Serializable;

public class Asset implements Serializable {
/*
* dispId: '49366',
  comment: 'DISCO DURO EXTERNO/MARCA WD/MODELO WD320\r\n',
  template: 'ActivoDesaparecido',
  smallLocation: 'Oficina de Secretaría',
  fullLocation: 'Unidad de Computación > Oficinas Computación > Oficina de Secretaría' }*/
    private String dispId;
    private String comment;
    private String template;
    private String smallLocation;
    private String fullLocation;

    public Asset(String dispId, String name, String comment, String template, String smallLocation, String fullLocation) {
        this.dispId = dispId;
        this.comment = comment;
        this.template = template;
        this.smallLocation = smallLocation;
        this.fullLocation = fullLocation;
    }

    public String getDispId() {
        return dispId;
    }

    public void setDispId(String dispId) {
        this.dispId = dispId;
    }



    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getTemplate() {
        return template;
    }

    public void setTemplate(String template) {
        this.template = template;
    }

    public String getSmallLocation() {
        return smallLocation;
    }

    public void setSmallLocation(String smallLocation) {
        this.smallLocation = smallLocation;
    }

    public String getFullLocation() {
        return fullLocation;
    }

    public void setFullLocation(String fullLocation) {
        this.fullLocation = fullLocation;
    }

    @Override
    public String toString() {
        return "Asset{" +
                "dispId='" + dispId + '\'' +

                ", comment='" + comment + '\'' +
                ", template='" + template + '\'' +
                ", smallLocation='" + smallLocation + '\'' +
                ", fullLocation='" + fullLocation + '\'' +
                '}';
    }
}
