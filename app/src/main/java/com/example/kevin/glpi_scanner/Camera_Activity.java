package com.example.kevin.glpi_scanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.google.zxing.Result;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class Camera_Activity extends AppCompatActivity {

    private ZXingScannerView scannerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera);


        scannerView = new ZXingScannerView(this);
        scannerView.setResultHandler(new zxingScanner());
        setContentView(scannerView);
        scannerView.startCamera();


    }



    class zxingScanner implements ZXingScannerView.ResultHandler{

        @Override
        public void handleResult(Result result) {

            String code = result.getText();
            setContentView(R.layout.activity_camera);
            scannerView.stopCamera();
            Intent resultIntent = new Intent(getApplicationContext(), Camera_Activity.class);
            resultIntent.putExtra("code", code);
            setResult(RESULT_OK, resultIntent);
            finish();

        }
    }
}
