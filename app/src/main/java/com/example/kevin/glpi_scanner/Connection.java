package com.example.kevin.glpi_scanner;

public class Connection {


    private String ipAddress;
    private String port;


    public Connection() {
        this.ipAddress = "http://172.24.160.5";
        this.port = ":1152/";
    }

    public String searchAsset(String code){return this.ipAddress+this.port+"search/"+code; }

    public String getIpAddress() {return ipAddress; }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }

    public String getPort() {return port; }

    public void setPort(String port) {
        this.port = port;
    }
}
